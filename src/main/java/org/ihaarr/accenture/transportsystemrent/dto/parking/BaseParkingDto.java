package org.ihaarr.accenture.transportsystemrent.dto.parking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.utils.Instanceable;

public class BaseParkingDto implements Instanceable<Parking> {
    @JsonProperty(value = "id")
    @Getter
    @Setter
    @Accessors(chain = true)
    protected Long id;

    public BaseParkingDto(Parking parking) {
        this.id = parking.getId();
    }
    public BaseParkingDto(){}

    @Override
    @JsonIgnore
    public Parking getInstance() {
        return new Parking(this.id);
    }
}
