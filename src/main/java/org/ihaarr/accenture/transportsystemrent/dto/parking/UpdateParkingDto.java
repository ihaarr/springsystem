package org.ihaarr.accenture.transportsystemrent.dto.parking;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ihaarr.accenture.transportsystemrent.entity.parking.ParkingStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;

import javax.validation.Valid;

public class UpdateParkingDto extends BaseRequiredParkingDto {
    @JsonProperty(value = "longitude", required = true)
    @Getter
    @Setter
    protected double longitude;

    @JsonProperty(value = "latitude", required = true)
    @Getter
    @Setter
    protected double latitude;

    @JsonProperty(value = "permittedRadius")
    @Getter
    @Setter
    @Valid
    protected double permittedRadius;

    @JsonProperty(value = "transportType")
    @Getter
    @Setter
    @Valid
    protected TransportType transportType;

    @JsonProperty(value = "status")
    @Getter
    @Setter
    @Valid
    protected ParkingStatus status;
}
