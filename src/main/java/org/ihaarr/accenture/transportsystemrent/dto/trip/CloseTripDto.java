package org.ihaarr.accenture.transportsystemrent.dto.trip;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class CloseTripDto {
    @JsonProperty(value = "tripId", required = true)
    @Getter
    @Setter
    protected Long tripId;

    @JsonProperty(value = "parkingId", required = true)
    @Getter
    @Setter
    protected Long parkingId;
}
