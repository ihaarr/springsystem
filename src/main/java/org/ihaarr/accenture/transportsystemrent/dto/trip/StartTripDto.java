package org.ihaarr.accenture.transportsystemrent.dto.trip;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class StartTripDto {
    @JsonProperty(value = "transportId", required = true)
    @Getter
    @Setter
    protected Long transportId;
}
