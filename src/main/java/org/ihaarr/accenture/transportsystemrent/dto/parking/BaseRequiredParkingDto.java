package org.ihaarr.accenture.transportsystemrent.dto.parking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.utils.Instanceable;

public class BaseRequiredParkingDto implements Instanceable<Parking> {
    @JsonProperty(value = "id", required = true)
    @Getter
    @Setter
    @Accessors(chain = true)
    protected Long id;

    public BaseRequiredParkingDto(Parking parking) {
        this.id = parking.getId();
    }
    public BaseRequiredParkingDto(){}

    @Override
    @JsonIgnore
    public Parking getInstance() {
        return new Parking(this.id);
    }
}
