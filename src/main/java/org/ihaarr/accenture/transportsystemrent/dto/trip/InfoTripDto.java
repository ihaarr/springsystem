package org.ihaarr.accenture.transportsystemrent.dto.trip;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.ihaarr.accenture.transportsystemrent.entity.trip.TripStatus;

@AllArgsConstructor
public class InfoTripDto {
    @JsonProperty("id")
    @Getter
    @Setter
    protected Long id;
    @JsonProperty("status")
    @Getter
    @Setter
    protected TripStatus status;
}
