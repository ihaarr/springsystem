package org.ihaarr.accenture.transportsystemrent.dto.user;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;

import java.math.BigDecimal;

@Accessors(chain = true)
public class CreateUserDto extends BaseUserDto{
    @Getter
    @Setter
    protected String login;

    @Getter
    @Setter
    protected String password;

    @Getter
    @Setter
    protected UserRole role;

    @Getter
    @Setter
    protected BigDecimal balance;

    @Override
    public User getInstance() {
        return new User()
                .setLogin(this.login)
                .setPassword(this.password)
                .setBalance(this.balance)
                .setRole(this.role);
    }

}
