package org.ihaarr.accenture.transportsystemrent.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;
import org.ihaarr.accenture.transportsystemrent.utils.Instanceable;

import java.math.BigDecimal;

public class BaseUserDto implements Instanceable<User> {
    @JsonProperty(value = "id")
    @Getter
    @Setter
    @Accessors(chain = true)
    protected Long id;

    public BaseUserDto(User user) {
        this.id = user.getId();
    }
    public BaseUserDto(){}

    @JsonIgnore
    @Override
    public User getInstance() {
        return new User(id, "", "", BigDecimal.valueOf(0), UserRole.EMPTY);
    }
}
