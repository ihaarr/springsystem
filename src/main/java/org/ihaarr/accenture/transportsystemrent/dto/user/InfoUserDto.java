package org.ihaarr.accenture.transportsystemrent.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;

import java.math.BigDecimal;

@Accessors(chain = true)
public class InfoUserDto extends BaseUserDto {
    @JsonProperty(value = "balance", required = true)
    @Getter
    @Setter
    protected BigDecimal balance;

    @JsonProperty(value = "login", required = true)
    @Getter
    @Setter
    protected String login;

    @JsonProperty(value = "role", required = true)
    @Getter
    @Setter
    protected UserRole role;

    @JsonIgnore
    @Override
    public User getInstance() {
        return new User(this.id, this.login, "", this.balance, UserRole.EMPTY);
    }
}
