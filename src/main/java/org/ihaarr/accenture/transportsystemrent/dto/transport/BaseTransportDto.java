package org.ihaarr.accenture.transportsystemrent.dto.transport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.utils.Instanceable;

public class BaseTransportDto implements Instanceable<Transport> {
    @JsonProperty(value = "id")
    @Getter
    @Setter
    @Accessors(chain = true)
    protected Long id;

    public BaseTransportDto(Transport transport) {
        this.id = transport.getId();
    }
    public BaseTransportDto(){}

    @Override
    @JsonIgnore
    public Transport getInstance() {
        Transport transport = new Transport();
        transport.setId(this.id);
        return transport;
    }
}
