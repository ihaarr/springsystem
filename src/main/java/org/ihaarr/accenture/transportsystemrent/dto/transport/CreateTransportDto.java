package org.ihaarr.accenture.transportsystemrent.dto.transport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportCondition;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.springframework.data.relational.core.sql.In;

@Accessors(chain = true)
public class CreateTransportDto extends BaseTransportDto{
    @JsonProperty(value = "longitude", required = true)
    @Getter
    @Setter
    protected double longitude;

    @JsonProperty(value = "latitude", required = true)
    @Getter
    @Setter
    protected double latitude;

    @JsonProperty(value = "type", required = true)
    @Getter
    @Setter
    protected TransportType type;

    @JsonProperty(value = "parkingId", required = true)
    @Getter
    @Setter
    protected Long parkingId;

    @Override
    @JsonIgnore
    public Transport getInstance() {
        return new Transport()
                .setId(this.id)
                .setStatus(TransportStatus.FREE)
                .setCondition(TransportCondition.EXCELLENT)
                .setCharge(this.type == TransportType.ELECTRIC_SCOOTER ? 100 : null)
                .setMaxSpeed(this.type == TransportType.ELECTRIC_SCOOTER ? 25 : null)
                .setType(this.type)
                .setParking(new Parking(parkingId))
                .setLongitude(this.longitude)
                .setLatitude(this.latitude);
    }
}
