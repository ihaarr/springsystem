package org.ihaarr.accenture.transportsystemrent.dto.parking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.parking.ParkingStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;

@Accessors(chain = true)
public class CreateParkingDto extends BaseParkingDto {
    @JsonProperty(value = "longitude", required = true)
    @Getter
    @Setter
    protected double longitude;

    @JsonProperty(value = "latitude", required = true)
    @Getter
    @Setter
    protected double latitude;

    @JsonProperty(value = "permittedRadius", required = true)
    @Getter
    @Setter
    protected double permittedRadius;

    @JsonProperty(value = "transportType")
    @Getter
    @Setter
    protected TransportType transportType;

    @JsonProperty(value = "name", required = true)
    @Getter
    @Setter
    protected String name;

    @Override
    @JsonIgnore
    public Parking getInstance() {
        return new Parking()
                .setId(this.id)
                .setName(this.name)
                .setStatus(ParkingStatus.ACTIVE)
                .setTransportType(this.transportType != null ? this.transportType : TransportType.ALL)
                .setPermittedRadius(this.permittedRadius)
                .setLongitude(this.longitude)
                .setLatitude(this.latitude);
    }
}
