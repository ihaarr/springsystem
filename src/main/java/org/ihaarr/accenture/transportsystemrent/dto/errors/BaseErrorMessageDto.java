package org.ihaarr.accenture.transportsystemrent.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public class BaseErrorMessageDto {
    @Getter
    @Setter
    private HttpStatus status;

    @Getter
    @Setter
    private String message;
}
