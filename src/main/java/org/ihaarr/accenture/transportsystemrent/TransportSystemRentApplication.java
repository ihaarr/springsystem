package org.ihaarr.accenture.transportsystemrent;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.bot.telegram.TelegramBot;
import org.ihaarr.accenture.transportsystemrent.configuration.MainConfig;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;
import java.util.ResourceBundle;

@Slf4j
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class TransportSystemRentApplication {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
		SpringApplication.run(TransportSystemRentApplication.class, args);
		try {
			TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
			botsApi.registerBot(new TelegramBot());
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}
}
