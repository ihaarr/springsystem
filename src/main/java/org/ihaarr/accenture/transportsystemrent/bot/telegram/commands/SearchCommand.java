package org.ihaarr.accenture.transportsystemrent.bot.telegram.commands;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.repository.TransportRepository;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
public class SearchCommand extends BotCommand {
    private TransportRepository transportRepository;

    public SearchCommand() {
        super("search", "search free transports");
        transportRepository = (TransportRepository) SpringContext.get().getBean("transportRepository");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        TransportType type = TransportType.ALL;
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setParseMode("HTML");
        if(strings.length > 0) {
            try {
                type = TransportType.valueOf(strings[0].toUpperCase());
            } catch (IllegalArgumentException ignored) {
                answer.setText("Неверно введен тип транспорта");
                try {
                    absSender.execute(answer);
                } catch (TelegramApiException e) {
                    log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
                }
                return;
            }
        }

        Iterable<Transport> list;
        if(type == TransportType.ALL) {
            list = transportRepository.getFreeTransports();
        } else {
            list = transportRepository.getFreeTransportsByType(type);
        }
        StringBuilder message = new StringBuilder();
        list.forEach(transport -> {
            message.append("ID: ").append(transport.getId()).append("\n")
                    .append("TYPE: ").append(transport.getType()).append("\n")
                    .append("===================\n");
        });

        try {
            answer.setText(message.toString());
            absSender.execute(answer);
        } catch (TelegramApiException e) {
            log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
        }
    }
}
