package org.ihaarr.accenture.transportsystemrent.bot.telegram.commands;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.dto.trip.StartTripDto;
import org.ihaarr.accenture.transportsystemrent.service.TripServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TripService;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Date;

@Slf4j
public class TripStartCommand extends BotCommand {
    private TripService tripService;
    public TripStartCommand() {
        super("trip", "start trip");
        tripService = (TripService) SpringContext.get().getBean("tripServiceImpl");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setParseMode("HTML");
        if(strings.length < 1) {
            answer.setText("Введите идентификатор транспорта");
            try {
                absSender.execute(answer);
            } catch (TelegramApiException e) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
            }
            return;
        }

        Long id;
        try {
            id = Long.valueOf(strings[0]);
        } catch (Exception ex) {
            answer.setText("Неправильно введен идентификатор транспорта");
            try {
                absSender.execute(answer);
            } catch (TelegramApiException e) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
            }
            return;
        }

        StartTripDto dto = new StartTripDto();
        dto.setTransportId(id);
        Long tripId = null;
        try {
            tripId = this.tripService.start(dto).getId();
        } catch (Exception e) {
            answer.setText(e.getLocalizedMessage());
            try {
                absSender.execute(answer);
            } catch (TelegramApiException ex) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());

            }
            return;
        }

        answer.setText("Поездка начата в: " + new Date() + "\nID поездки: " + tripId);
        try {
            absSender.execute(answer);
        } catch (TelegramApiException ex) {
            log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), ex.getMessage());
        }
    }
}
