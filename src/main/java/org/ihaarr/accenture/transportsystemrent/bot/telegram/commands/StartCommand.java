package org.ihaarr.accenture.transportsystemrent.bot.telegram.commands;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.dto.user.CreateUserDto;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;
import org.ihaarr.accenture.transportsystemrent.repository.UserRepository;
import org.ihaarr.accenture.transportsystemrent.service.UserServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.UserService;
import org.ihaarr.accenture.transportsystemrent.utils.*;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.math.BigDecimal;

@Slf4j
public class StartCommand extends BotCommand {
    private UserService userService;
    private UserRepository userRepository;

    private BigDecimal startBalance;

    public StartCommand(BigDecimal startBalance) {
        super("start", "command to start to use");
        this.startBalance = startBalance;
        userService = (UserService) SpringContext.get().getBean("userServiceImpl");
        userRepository = (UserRepository) SpringContext.get().getBean("userRepository");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setParseMode("HTML");
        if (SecurityContext.get() != null && SecurityContext.get().getId() != -1) {
            try {
                answer.setText("Вы уже зарегестрированы");
                absSender.execute(answer);
            } catch (TelegramApiException e) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
            }
        } else {
            String login = user.getUserName() + user.getId();
            UserRole role = UserRole.TENANT;
            CreateUserDto userDto = new CreateUserDto();
            userDto.setLogin(login);
            userDto.setBalance(this.startBalance);
            userDto.setRole(role);
            userDto.setPassword(user.getFirstName());
            try {
                SecurityContext.set(UserContext.builder()
                        .setId(userService.createUser(userDto).getId())
                        .setBalance(this.startBalance)
                        .setRole(UserRole.TENANT).build());
                answer.setText("Вы успешно зарегестрированы");
            } catch (Exception e) {
                org.ihaarr.accenture.transportsystemrent.entity.user.User u =
                        userRepository.findByLogin(user.getUserName() + user.getId());
                SecurityContext.set(UserContext.builder()
                        .setId(u.getId())
                        .setBalance(u.getBalance())
                        .setRole(u.getRole()).build());
                answer.setText("Вы уже зарегестрированы");
            } finally {
                try {
                    absSender.execute(answer);
                } catch (Exception e) {
                    log.info("Can't send message to {}\n Error: {}", user.getId(), e.getMessage());
                }
            }
        }
    }
}
