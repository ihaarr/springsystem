package org.ihaarr.accenture.transportsystemrent.bot.telegram.commands;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.repository.TripRepository;
import org.ihaarr.accenture.transportsystemrent.repository.UserRepository;
import org.ihaarr.accenture.transportsystemrent.service.UserServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.UserService;
import org.ihaarr.accenture.transportsystemrent.utils.SecurityContext;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.math.BigDecimal;

@Slf4j
public class InfoCommand extends BotCommand {
    private UserService userService;
    private UserRepository userRepository;
    private TripRepository tripRepository;

    public InfoCommand() {
        super("info", "get info about user");
        userService = (UserService) SpringContext.get().getBean("userServiceImpl");
        userRepository = (UserRepository) SpringContext.get().getBean("userRepository");
        tripRepository = (TripRepository) SpringContext.get().getBean("tripRepository");
    }

    @Override
    public void execute(AbsSender absSender, User userTelegram, Chat chat, String[] strings) {
        org.ihaarr.accenture.transportsystemrent.entity.user.User user =
                userRepository.findById(SecurityContext.get().getId())
                        .orElse(new org.ihaarr.accenture.transportsystemrent.entity.user.User());
        StringBuilder message = new StringBuilder();
        message.append("Balance = ").append(user.getBalance()).append("\n").append("Trips: \n");
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setParseMode("HTML");
        tripRepository.findAllByUserId(user.getId()).forEach(trip -> {
            String name = trip.getEndParking() == null ? "" : trip.getEndParking().getName();
            BigDecimal totalPrice = trip.getTotalPrice() == null ? BigDecimal.valueOf(0) : trip.getTotalPrice();
            message.append("ID: ").append(trip.getId()).append("\n")
                    .append("STATUS: ").append(trip.getStatus()).append("\n")
                    .append("BEGIN_PARKING: ").append(trip.getBeginParking().getName()).append("\n")
                    .append("END_PARKING: ").append(name).append("\n")
                    .append("TOTAL_PRICE: ").append(totalPrice).append("\n")
                    .append("==================\n");
        });

        try {
            answer.setText(message.toString());
            absSender.execute(answer);
        } catch (TelegramApiException e) {
            log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
        }
    }
}
