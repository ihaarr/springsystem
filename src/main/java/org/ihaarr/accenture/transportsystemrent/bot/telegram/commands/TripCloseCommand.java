package org.ihaarr.accenture.transportsystemrent.bot.telegram.commands;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.dto.trip.CloseTripDto;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.ihaarr.accenture.transportsystemrent.repository.UserRepository;
import org.ihaarr.accenture.transportsystemrent.service.TripServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TripService;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
public class TripCloseCommand extends BotCommand {
    private UserRepository userRepository;
    private TripService tripService;
    public TripCloseCommand() {
        super("close", "close trip");
        userRepository = (UserRepository) SpringContext.get().getBean("userRepository");
        tripService = (TripService) SpringContext.get().getBean("tripServiceImpl");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setParseMode("HTML");
        if(strings.length < 2) {
            try {
                answer.setText("Введите id поедзки и id парковки");
                absSender.execute(answer);
            } catch (TelegramApiException e) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), e.getMessage());
            }

            return;
        }

        Long tripId = null;
        Long parkingId = null;
        try {
            tripId = Long.valueOf(strings[0]);
            parkingId = Long.valueOf(strings[1]);
        } catch (Exception e) {
            answer.setText("Введите корректные данные");
            try {
                absSender.execute(answer);
            } catch (TelegramApiException ex) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), ex.getMessage());
            }

            return;
        }
        CloseTripDto dto = new CloseTripDto();
        dto.setTripId(tripId);
        dto.setParkingId(parkingId);
        Trip trip = null;
        try {
            trip = tripService.close(dto);
        } catch (Exception e) {
            answer.setText(e.getLocalizedMessage());
            try {
                absSender.execute(answer);
            } catch (TelegramApiException ex) {
                log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), ex.getMessage());
            }

            return;
        }

        answer.setText("Итоговая сумма = " + trip.getTotalPrice() + "\n");
        try {
            absSender.execute(answer);
        } catch (TelegramApiException ex) {
            log.info("Can't send message to user with id = {}\n Error: {}", user.getId(), ex.getMessage());
        }
    }
}
