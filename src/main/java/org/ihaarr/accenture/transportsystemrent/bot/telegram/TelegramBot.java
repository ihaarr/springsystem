package org.ihaarr.accenture.transportsystemrent.bot.telegram;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.bot.telegram.commands.*;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.ihaarr.accenture.transportsystemrent.utils.SpringContext;
import org.ihaarr.accenture.transportsystemrent.utils.TelegramSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.objects.Update;

@Slf4j
public class TelegramBot extends TelegramLongPollingCommandBot {

    private TelegramSettings settings;

    public TelegramBot() {
        settings = (TelegramSettings) SpringContext.get().getBean("telegramSettings");
        this.register(new StartCommand(settings.getStartBalance()));
        this.register(new InfoCommand());
        this.register(new SearchCommand());
        this.register(new TripStartCommand());
        this.register(new TripCloseCommand());
    }

    @Override
    public String getBotUsername() {
        return settings.getName();
    }

    @Override
    public void processNonCommandUpdate(Update update) {

    }

    @Override
    public String getBotToken() {
        return settings.getToken();
    }
}
