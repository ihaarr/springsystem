package org.ihaarr.accenture.transportsystemrent.controller;

import org.ihaarr.accenture.transportsystemrent.dto.trip.CloseTripDto;
import org.ihaarr.accenture.transportsystemrent.dto.trip.StartTripDto;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.ihaarr.accenture.transportsystemrent.service.TripServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trip")
public class TripController {
    @Autowired
    private TripService tripService;

    @GetMapping("")
    public Iterable<Trip> getAll() {
        return tripService.findAll();
    }

    @PostMapping("/start")
    public Trip startTrip(@RequestBody @Validated StartTripDto startTripDto) throws Exception {
        return tripService.start(startTripDto);
    }

    @PostMapping("/close")
    public Trip closeTrip(@RequestBody @Validated CloseTripDto closeTripDto) throws Exception {
        return tripService.close(closeTripDto);
    }
}
