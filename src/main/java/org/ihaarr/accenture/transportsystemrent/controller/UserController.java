package org.ihaarr.accenture.transportsystemrent.controller;

import org.ihaarr.accenture.transportsystemrent.dto.user.*;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.UserNotExistsException;
import org.ihaarr.accenture.transportsystemrent.service.UserServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(value="/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public InfoUserDto getCurrentUser() throws UserNotExistsException {
        return userService.getCurrentUser();
    }
}
