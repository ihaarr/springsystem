package org.ihaarr.accenture.transportsystemrent.controller;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.dto.parking.CreateParkingDto;
import org.ihaarr.accenture.transportsystemrent.dto.parking.UpdateParkingDto;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/parking")
@Slf4j
public class ParkingController {
    @Autowired
    private ParkingService parkingService;

    @GetMapping("")
    public Iterable<Parking> getAll() {
        return parkingService.getAll();
    }

    @PostMapping("")
    public Parking save(@RequestBody @Validated CreateParkingDto parkingZone) {
        return parkingService.save(parkingZone);
    }

    @PutMapping("")
    public Parking update(@RequestBody @Validated UpdateParkingDto dto) throws Exception {
        return parkingService.update(dto);
    }

    @DeleteMapping("/{id}")
    public Parking delete(@PathVariable @Validated Long id) throws ParkingNotExistsException {
        return parkingService.delete(id);
    }
}
