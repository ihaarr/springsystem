package org.ihaarr.accenture.transportsystemrent.controller;

import org.ihaarr.accenture.transportsystemrent.dto.transport.CreateTransportDto;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportCondition;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TransportNotExistsException;
import org.ihaarr.accenture.transportsystemrent.repository.ParkingRepository;
import org.ihaarr.accenture.transportsystemrent.repository.TransportRepository;
import org.ihaarr.accenture.transportsystemrent.service.TransportServiceImpl;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transport")
public class TransportController {
    @Autowired
    private TransportRepository repo;

    @Autowired
    private TransportService transportService;

    @Autowired
    private ParkingRepository parkingRepository;

    @GetMapping("/type/{type}")
    public Iterable<Transport> getTransportsByType(@PathVariable @Validated TransportType type) {
        return transportService.getTransportsByType(type);
    }

    @GetMapping("/condition/{condition}")
    public Iterable<Transport> getTransportsByCondition(@PathVariable @Validated TransportCondition condition) {
        return transportService.getTransportsByCondition(condition);
    }

    @GetMapping("/status/{status}")
    public Iterable<Transport> getTransportsByStatus(@PathVariable @Validated TransportStatus status) {
       return transportService.getTransportsByStatus(status);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Transport> getAll() {
        return transportService.getAll();
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Transport save(@RequestBody @Validated CreateTransportDto transport) throws Exception {
        return transportService.save(transport);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Transport delete(@PathVariable @Validated Long id) throws TransportNotExistsException {
        return transportService.delete(id);
    }
}
