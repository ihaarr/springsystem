package org.ihaarr.accenture.transportsystemrent.service;

import org.ihaarr.accenture.transportsystemrent.dto.parking.CreateParkingDto;
import org.ihaarr.accenture.transportsystemrent.dto.parking.UpdateParkingDto;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.parking.ParkingStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;
import org.ihaarr.accenture.transportsystemrent.repository.ParkingRepository;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Optional;

@Service
public class ParkingServiceImpl implements ParkingService {
    @Autowired
    private ParkingRepository parkingRepository;

    public Iterable<Parking> getAll() {
        return parkingRepository.findAll();
    }

    public Parking save(CreateParkingDto parkingZone){
            return parkingRepository.save(parkingZone.getInstance());
    }

    public Parking update(UpdateParkingDto dto) throws Exception {
        Optional<Parking> parkingOptional = parkingRepository.findById(dto.getId());
        if(!parkingOptional.isPresent()) {
            throw new ParkingNotExistsException();
        } else {
            Parking parking = parkingOptional.get();
            for (Field field : dto.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object value = field.get(dto);
                if(value != null) {
                    switch(field.getName()) {
                        case "longitude":
                            parking.setLongitude((double) value);
                            break;
                        case "latitude":
                            parking.setLatitude((double) value);
                            break;
                        case "permittedRadius":
                            parking.setPermittedRadius((double) value);
                            break;
                        case "transportType":
                            parking.setTransportType((TransportType) value);
                            break;
                        case "status":
                            parking.setStatus((ParkingStatus) value);
                            break;
                    }
                }
            }

            return parkingRepository.save(parking);
        }
    }

    public Parking delete(Long id) throws ParkingNotExistsException {
        Optional<Parking> parkingOptional = parkingRepository.findById(id);
        if(!parkingOptional.isPresent()) {
            throw new ParkingNotExistsException();
        } else {
            Parking parking = parkingOptional.get();
            parking.setStatus(ParkingStatus.CLOSED);
            return parkingRepository.save(parking);
        }
    }
}
