package org.ihaarr.accenture.transportsystemrent.service;

import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.dto.trip.CloseTripDto;
import org.ihaarr.accenture.transportsystemrent.dto.trip.InfoTripDto;
import org.ihaarr.accenture.transportsystemrent.dto.trip.StartTripDto;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.ihaarr.accenture.transportsystemrent.entity.trip.TripStatus;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;
import org.ihaarr.accenture.transportsystemrent.exceptions.*;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TransportNotExistsException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TripNotExistsException;
import org.ihaarr.accenture.transportsystemrent.repository.ParkingRepository;
import org.ihaarr.accenture.transportsystemrent.repository.TransportRepository;
import org.ihaarr.accenture.transportsystemrent.repository.TripRepository;
import org.ihaarr.accenture.transportsystemrent.repository.UserRepository;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TripService;
import org.ihaarr.accenture.transportsystemrent.utils.SecurityContext;
import org.ihaarr.accenture.transportsystemrent.utils.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TripServiceImpl implements TripService {
    @Autowired
    private TransportRepository transportRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private TripRepository repo;

    private final BigDecimal basePriceForVelo;
    private final BigDecimal pricePerMinuteForVelo;
    private final BigDecimal basePriceForScooter;
    private final BigDecimal pricePerMinuteForScooter;

    public TripServiceImpl(@Value("${transport.types.velo.basePrice}") BigDecimal basePriceForVelo,
                           @Value("${transport.types.velo.pricePerMinute}") BigDecimal pricePerMinuteForVelo,
                           @Value("${transport.types.scooter.basePrice}") BigDecimal basePriceForScooter,
                           @Value("${transport.types.scooter.pricePerMinute}") BigDecimal pricePerMinuteForScooter) {
        this.basePriceForVelo = basePriceForVelo;
        this.pricePerMinuteForVelo = pricePerMinuteForVelo;
        this.basePriceForScooter = basePriceForScooter;
        this.pricePerMinuteForScooter = pricePerMinuteForScooter;
    }


    public Iterable<Trip> findAll() {
        if(SecurityContext.get().getRole() == UserRole.ADMIN) {
            return repo.findAll();
        } else {
            return repo.findAllByUserId(SecurityContext.get().getId());
        }
    }

    @Transactional
    public Trip start(StartTripDto startTripDto) throws Exception {
        Transport transport = transportRepository.findById(startTripDto.getTransportId())
                .orElseThrow(TransportNotExistsException::new);
        if (transport.getStatus() != TransportStatus.FREE) {
            throw new TransportIsNotAvaiableException();
        }
        UserContext userContext = SecurityContext.get();
        BigDecimal basePrice = null;
        switch (transport.getType()) {
            case ELECTRIC_SCOOTER:
                basePrice = basePriceForScooter;
                break;
            case BICYCLE:
                basePrice = basePriceForVelo;
                break;
        }

        if (userContext.getBalance().compareTo(BigDecimal.valueOf(0)) <= 0) {
            throw new NotEnoughBalanceException();
        }
        User user = userRepository.findById(userContext.getId()).get();
        transport.setStatus(TransportStatus.BUSY);
        transportRepository.save(transport);
        Trip trip = new Trip();
        trip.setBeginRent(new Date());
        trip.setTransport(transport);
        trip.setBeginParking(transport.getParking());
        trip.setStatus(TripStatus.CREATED);
        trip.setUser(user);
        trip.setBeginParking(transport.getParking());
        Trip saved = repo.save(trip);
        return saved;
    }

    @Transactional
    public Trip close(CloseTripDto closeTripDto) throws Exception {
        Trip trip = repo.findById(closeTripDto.getTripId()).orElseThrow(TripNotExistsException::new);

        if (trip.getStatus() != TripStatus.CREATED) {
            throw new AccessToClosedTripException();
        }

        Long userId = SecurityContext.get().getId();;

        if (!trip.getUser().getId().equals(userId)) {
            throw new AccessToDifferentUserTripException();
        }

        Parking parking = parkingRepository.findById(closeTripDto.getParkingId())
                .orElseThrow(ParkingNotExistsException::new);

        Transport transport = trip.getTransport();

        if (parking.getTransportType() != transport.getType() && parking.getTransportType() != TransportType.ALL) {
            throw new InvalidTransportTypeOnParkingException();
        }

        User user = userRepository.findById(userId).get();

        Date now = new Date();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - trip.getBeginRent().getTime());
        BigDecimal basePrice = null;
        BigDecimal pricePerMinute = null;
        switch (transport.getType()) {
            case ELECTRIC_SCOOTER:
                basePrice = basePriceForScooter;
                pricePerMinute = this.pricePerMinuteForScooter;
                break;
            case BICYCLE:
                basePrice = basePriceForVelo;
                pricePerMinute = this.pricePerMinuteForVelo;
                break;
        }
        BigDecimal finalPrice = basePrice.add(pricePerMinute.multiply(BigDecimal.valueOf(minutes)));
        if (user.getBalance().compareTo(finalPrice) < 0) {
            throw new NotEnoughBalanceException();
        }

        user.setBalance(user.getBalance().subtract(finalPrice));
        userRepository.save(user);
        transport.setParking(parking);
        transport.setLongitude(parking.getLongitude() + Math.random() * 0.001);
        transport.setLatitude(parking.getLatitude() + Math.random() * 0.001);
        transport.setStatus(TransportStatus.FREE);
        transportRepository.save(transport);
        trip.setEndParking(parking);
        trip.setStatus(TripStatus.FINISHED);
        trip.setEndRent(now);
        trip.setTotalPrice(finalPrice);
        return repo.save(trip);
    }
}
