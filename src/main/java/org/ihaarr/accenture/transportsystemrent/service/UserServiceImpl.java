package org.ihaarr.accenture.transportsystemrent.service;

import org.ihaarr.accenture.transportsystemrent.dto.user.CreateUserDto;
import org.ihaarr.accenture.transportsystemrent.dto.user.InfoUserDto;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.UserNotExistsException;
import org.ihaarr.accenture.transportsystemrent.repository.UserRepository;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.UserService;
import org.ihaarr.accenture.transportsystemrent.utils.SecurityContext;
import org.ihaarr.accenture.transportsystemrent.utils.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    public InfoUserDto getCurrentUser() throws UserNotExistsException {
        InfoUserDto infoUserDto = new InfoUserDto();
        UserContext context = SecurityContext.get();
        infoUserDto.setLogin(context.getLogin())
                .setRole(context.getRole())
                .setBalance(context.getBalance())
                .setId(context.getId())
        ;
        return infoUserDto;
    }

    public User createUser(CreateUserDto userDto) {
        return userRepository.save(userDto.getInstance());
    }
}
