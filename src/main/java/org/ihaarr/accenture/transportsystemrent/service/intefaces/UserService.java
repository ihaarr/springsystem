package org.ihaarr.accenture.transportsystemrent.service.intefaces;

import org.ihaarr.accenture.transportsystemrent.dto.user.CreateUserDto;
import org.ihaarr.accenture.transportsystemrent.dto.user.InfoUserDto;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.UserNotExistsException;

public interface UserService {
    public InfoUserDto getCurrentUser() throws UserNotExistsException;
    public User createUser(CreateUserDto userDto);
}
