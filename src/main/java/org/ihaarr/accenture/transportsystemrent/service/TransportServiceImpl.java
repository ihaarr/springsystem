package org.ihaarr.accenture.transportsystemrent.service;

import org.ihaarr.accenture.transportsystemrent.dto.transport.CreateTransportDto;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportCondition;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.exceptions.InvalidTransportTypeOnParkingException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TransportNotExistsException;
import org.ihaarr.accenture.transportsystemrent.repository.ParkingRepository;
import org.ihaarr.accenture.transportsystemrent.repository.TransportRepository;
import org.ihaarr.accenture.transportsystemrent.service.intefaces.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class TransportServiceImpl implements TransportService {
    @Autowired
    private TransportRepository transportRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    public Iterable<Transport> getAll() {
        return transportRepository.findAll();
    }

    public Iterable<Transport> getTransportsByStatus(TransportStatus status) {
        return transportRepository.getTransportsByStatus(status);
    }

    public Iterable<Transport> getTransportsByCondition(TransportCondition condition) {
        return transportRepository.getTransportsByCondition(condition);
    }

    public Iterable<Transport> getTransportsByType(TransportType type) {
        return transportRepository.getTransportsByType(type);
    }

    public Transport save(CreateTransportDto transport) throws Exception {
        Optional<Parking> parking = parkingRepository.findById(transport.getParkingId());
        if(!parking.isPresent()) {
            throw new ParkingNotExistsException();
        } else {
            Transport tr = transport.getInstance();
            Parking park = parking.get();
            if(park.getTransportType() != TransportType.ALL && tr.getType() != park.getTransportType()) {
                throw new InvalidTransportTypeOnParkingException();
            }
            tr.setParking(park);
            return transportRepository.save(tr);
        }
    }

    public Transport delete(Long id) throws TransportNotExistsException {
        Optional<Transport> transport = transportRepository.findById(id);
        if(!transport.isPresent()) {
            throw new TransportNotExistsException();
        } else {
            Transport tr = transport.get();
            tr.setStatus(TransportStatus.CLOSED);
            return transportRepository.save(tr);
        }
    }
}
