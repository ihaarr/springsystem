package org.ihaarr.accenture.transportsystemrent.service.intefaces;

import org.ihaarr.accenture.transportsystemrent.dto.trip.CloseTripDto;
import org.ihaarr.accenture.transportsystemrent.dto.trip.StartTripDto;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;

public interface TripService {
    public Iterable<Trip> findAll();
    public Trip start(StartTripDto startTripDto) throws Exception;
    public Trip close(CloseTripDto closeTripDto) throws Exception;
}
