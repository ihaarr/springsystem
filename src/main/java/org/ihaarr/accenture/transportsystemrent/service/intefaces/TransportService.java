package org.ihaarr.accenture.transportsystemrent.service.intefaces;

import org.ihaarr.accenture.transportsystemrent.dto.transport.CreateTransportDto;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportCondition;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TransportNotExistsException;

public interface TransportService {
    public Iterable<Transport> getAll();
    public Iterable<Transport> getTransportsByStatus(TransportStatus status);
    public Iterable<Transport> getTransportsByCondition(TransportCondition condition);
    public Iterable<Transport> getTransportsByType(TransportType type);
    public Transport save(CreateTransportDto transport) throws Exception;
    public Transport delete(Long id) throws TransportNotExistsException;
}
