package org.ihaarr.accenture.transportsystemrent.service.intefaces;

import org.ihaarr.accenture.transportsystemrent.dto.parking.CreateParkingDto;
import org.ihaarr.accenture.transportsystemrent.dto.parking.UpdateParkingDto;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;

public interface ParkingService {
    public Parking save(CreateParkingDto parkingZone);
    public Parking update(UpdateParkingDto dto) throws Exception;
    public Parking delete(Long id) throws ParkingNotExistsException;
    public Iterable<Parking> getAll();
}
