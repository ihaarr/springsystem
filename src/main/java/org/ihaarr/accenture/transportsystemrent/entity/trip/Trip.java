package org.ihaarr.accenture.transportsystemrent.entity.trip;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="trip")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @JoinColumn(name="transport_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Getter
    @Setter
    private Transport transport;

    @JoinColumn(name="user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Getter
    @Setter
    private User user;

    @Column(name="begin_rent")
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date beginRent;

    @Column(name="end_rent")
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date endRent;

    @JoinColumn(name="begin_parking_id")
    @OneToOne(fetch = FetchType.LAZY)
    @Getter
    @Setter
    private Parking beginParking;

    @JoinColumn(name="end_parking_id")
    @OneToOne(fetch = FetchType.LAZY)
    @Getter
    @Setter
    private Parking endParking;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    private TripStatus status;

    @Column(name="total_price")
    @Getter
    @Setter
    private BigDecimal totalPrice;

    public Trip(){}
}
