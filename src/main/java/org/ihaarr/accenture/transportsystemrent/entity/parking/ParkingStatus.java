package org.ihaarr.accenture.transportsystemrent.entity.parking;

public enum ParkingStatus {
    ACTIVE,
    CLOSED
}
