package org.ihaarr.accenture.transportsystemrent.entity.trip;

public enum TripStatus {
    CREATED,
    FINISHED
}
