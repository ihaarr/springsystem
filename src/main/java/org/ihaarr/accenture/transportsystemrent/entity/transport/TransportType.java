package org.ihaarr.accenture.transportsystemrent.entity.transport;

public enum TransportType {
    ALL,
    BICYCLE,
    ELECTRIC_SCOOTER,
}
