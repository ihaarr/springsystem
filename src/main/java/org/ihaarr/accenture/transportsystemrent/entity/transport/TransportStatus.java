package org.ihaarr.accenture.transportsystemrent.entity.transport;

public enum TransportStatus {
    FREE,
    BUSY,
    CLOSED
}
