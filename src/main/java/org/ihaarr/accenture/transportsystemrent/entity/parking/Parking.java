package org.ihaarr.accenture.transportsystemrent.entity.parking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="parking")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Accessors(chain = true)
public class Parking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column(name="name")
    @Getter
    @Setter
    private String name;

    @Column(name="longitude")
    @Getter
    @Setter
    private double longitude;


    @Column(name="latitude")
    @Getter
    @Setter
    private double latitude;

    @Column(name="permitted_radius")
    @Getter
    @Setter
    private double permittedRadius;

    @Column(name="transport_type")
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    private TransportType transportType;

    @Column
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    private ParkingStatus status;

    public Parking(Long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "parking")
    private List<Transport> transportId;
}
