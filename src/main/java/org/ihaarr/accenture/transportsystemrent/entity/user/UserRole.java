package org.ihaarr.accenture.transportsystemrent.entity.user;

public enum UserRole {
    EMPTY,
    TENANT,
    ADMIN
}
