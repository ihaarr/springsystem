package org.ihaarr.accenture.transportsystemrent.entity.transport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.springframework.lang.NonNull;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="transport")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Accessors(chain = true)
public class Transport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column(name="type")
    @Enumerated(EnumType.STRING)
    @NonNull
    @Getter
    @Setter
    private TransportType type;

    @Column(name="condition")
    @Enumerated(EnumType.STRING)
    @NonNull
    @Getter
    @Setter
    private TransportCondition condition;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    @NonNull
    private TransportStatus status;

    @Column(name="longitude")
    @Getter
    @Setter
    private double longitude;

    @Column(name="latitude")
    @Getter
    @Setter
    private double latitude;

    @JoinColumn(name="parking_id")
    @NonNull
    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    private Parking parking;

    @Column(name="charge")
    @Getter
    @Setter
    private Integer charge;

    @Column(name="max_speed")
    @Getter
    @Setter
    private Integer maxSpeed;

    @OneToMany(mappedBy = "transport", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Trip> trip;

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        return this.type.equals(((Transport) obj).getType());
    }

}
