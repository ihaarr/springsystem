package org.ihaarr.accenture.transportsystemrent.entity.transport;

public enum TransportCondition {
    EXCELLENT,
    GOOD,
    FAIR,
}
