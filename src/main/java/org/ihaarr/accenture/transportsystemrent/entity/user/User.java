package org.ihaarr.accenture.transportsystemrent.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name="users")
@Accessors(chain = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column(name="login", nullable = false)
    @Getter
    @Setter
    private String login;

    @Column(name="password", nullable = false)
    @Getter
    @Setter
    private String password;

    @Column(name="balance", nullable = false)
    @Getter
    @Setter
    private BigDecimal balance;

    @Column(name="role", nullable = false)
    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @OneToMany(mappedBy = "user")
    private List<Trip> trips;

    public User(){}
    public User(Long id, String login, String password, BigDecimal balance, UserRole role) {
        this.login = login;
        this.password = password;
        this.id = id;
        this.balance = balance;
        this.role = role;
    }

    @Override
    public String toString() {
        return "Id = " + id + " login = " + login + " password = " + password + " balance = " + balance;
    }
}
