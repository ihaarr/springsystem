package org.ihaarr.accenture.transportsystemrent.utils;

public interface Instanceable<T> {
    T getInstance();
}
