package org.ihaarr.accenture.transportsystemrent.utils;

import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;

import java.math.BigDecimal;

public class UserContextBuilder {
    private UserContext user;

    public UserContextBuilder() {
        this.user = new UserContext();
    }

    public UserContextBuilder setId(Long id) {
        this.user.setId(id);
        return this;
    }

    public UserContextBuilder setLogin(String login) {
        this.user.setLogin(login);
        return this;
    }

    public UserContextBuilder setRole(UserRole role) {
        this.user.setRole(role);
        return this;
    }

    public UserContextBuilder setBalance(BigDecimal balance) {
        this.user.setBalance(balance);
        return this;
    }

    public UserContext build() {
        return user;
    }

    public UserContextBuilder reset() {
        this.user = new UserContext();
        return this;
    }

}
