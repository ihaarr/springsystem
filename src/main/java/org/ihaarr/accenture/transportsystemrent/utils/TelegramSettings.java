package org.ihaarr.accenture.transportsystemrent.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TelegramSettings {
    @Value("${bots.telegram.token}")
    @Getter
    private String token;

    @Value("${bots.telegram.name}")
    @Getter
    private String name;

    @Value("${bots.telegram.startBalance}")
    @Getter
    private BigDecimal startBalance;
}
