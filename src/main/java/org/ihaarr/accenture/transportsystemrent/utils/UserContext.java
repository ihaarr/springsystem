package org.ihaarr.accenture.transportsystemrent.utils;

import lombok.Getter;
import lombok.Setter;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;

import java.math.BigDecimal;

public class UserContext {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private String login;
    @Getter
    @Setter
    private UserRole role;
    @Setter
    @Getter
    private BigDecimal balance;

    public static UserContextBuilder builder() {
        return new UserContextBuilder();
    }

}
