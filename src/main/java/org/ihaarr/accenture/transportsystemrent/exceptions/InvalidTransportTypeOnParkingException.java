package org.ihaarr.accenture.transportsystemrent.exceptions;

public class InvalidTransportTypeOnParkingException extends Exception{
    public InvalidTransportTypeOnParkingException() {
        super("Transport type doesn't equals to parking transport type");
    }

    public InvalidTransportTypeOnParkingException(String msg) {
        super(msg);
    }
}
