package org.ihaarr.accenture.transportsystemrent.exceptions;

public class TransportIsNotInRadiusException extends Exception{
    public TransportIsNotInRadiusException() {
        super("transport isn't in permitted radius of parking");
    }

    public TransportIsNotInRadiusException(String msg) {
        super(msg);
    }
}
