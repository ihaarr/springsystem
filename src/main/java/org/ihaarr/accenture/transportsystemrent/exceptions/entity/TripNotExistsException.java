package org.ihaarr.accenture.transportsystemrent.exceptions.entity;

public class TripNotExistsException extends Exception{
    public TripNotExistsException() {
        super("Trip doesn't exist");
    }

    public TripNotExistsException(String msg) {
        super(msg);
    }
}
