package org.ihaarr.accenture.transportsystemrent.exceptions;

public class TransportIsNotAvaiableException extends Exception{
    public TransportIsNotAvaiableException() {
        super("Transport isn't avaiable for trip");
    }

    public TransportIsNotAvaiableException(String msg) {
        super(msg);
    }
}
