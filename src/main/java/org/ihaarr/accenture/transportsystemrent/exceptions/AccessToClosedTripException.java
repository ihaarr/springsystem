package org.ihaarr.accenture.transportsystemrent.exceptions;

public class AccessToClosedTripException extends Exception{
    public AccessToClosedTripException() {
        super("attempt to close trip that already closed");
    }

    public AccessToClosedTripException(String msg) {
        super(msg);
    }
}
