package org.ihaarr.accenture.transportsystemrent.exceptions.entity;

public class TransportNotExistsException extends Exception{
    public TransportNotExistsException() {
        super("Transport doesn't exist");
    }

    public TransportNotExistsException(String msg) {
        super(msg);
    }
}
