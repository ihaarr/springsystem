package org.ihaarr.accenture.transportsystemrent.exceptions;

public class NotEnoughBalanceException extends Exception{
    public NotEnoughBalanceException() {
        super("Not enough money on balance");
    }

    public NotEnoughBalanceException(String msg) {
        super(msg);
    }
}
