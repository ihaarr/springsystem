package org.ihaarr.accenture.transportsystemrent.exceptions.entity;

public class UserNotExistsException extends Exception{
    public UserNotExistsException() {
        super("Такого пользователя не существует");
    }

    public UserNotExistsException(String msg) {
        super(msg);
    }
}
