package org.ihaarr.accenture.transportsystemrent.exceptions;

public class NoAccessException extends Exception{
    public NoAccessException(String msg) {
        super(msg);
    }

    public NoAccessException() {
        super("Недостаточно прав");
    }
}
