package org.ihaarr.accenture.transportsystemrent.exceptions;

public class AccessToDifferentUserTripException extends Exception{
    public AccessToDifferentUserTripException() {
        super("Attempt to close trip with different user");
    }

    public AccessToDifferentUserTripException(String msg) {
        super(msg);
    }
}
