package org.ihaarr.accenture.transportsystemrent.exceptions.entity;

public class ParkingNotExistsException extends Exception {
    public ParkingNotExistsException() {
        super("Parking doesn't exists");
    }

    public ParkingNotExistsException(String msg) {
        super(msg);
    }
}
