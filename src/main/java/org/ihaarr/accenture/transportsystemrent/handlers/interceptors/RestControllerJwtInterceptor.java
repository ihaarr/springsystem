package org.ihaarr.accenture.transportsystemrent.handlers.interceptors;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.ihaarr.accenture.transportsystemrent.entity.user.UserRole;
import org.ihaarr.accenture.transportsystemrent.exceptions.NoAccessException;
import org.ihaarr.accenture.transportsystemrent.utils.SecurityContext;
import org.ihaarr.accenture.transportsystemrent.utils.UserContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import io.jsonwebtoken.Jwts;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

@Component
@Slf4j
public class RestControllerJwtInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String auth = request.getHeader(HttpHeaders.AUTHORIZATION);
        auth = auth.split(" ")[1];
        Claims claims = Jwts.parser().setSigningKey("secret").parseClaimsJws(auth).getBody();
        UserContext userContext = UserContext.builder()
                .setId(Long.valueOf(claims.getSubject()))
                .setBalance(BigDecimal.valueOf(claims.get("balance", Double.class)))
                .setLogin(claims.get("login", String.class))
                .setRole(UserRole.valueOf(claims.get("role", String.class)))
                .build();
        SecurityContext.set(userContext);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                              Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, //
                                Object handler, Exception ex) throws Exception {
    }

}
