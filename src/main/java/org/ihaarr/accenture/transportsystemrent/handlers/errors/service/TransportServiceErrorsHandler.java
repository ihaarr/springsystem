package org.ihaarr.accenture.transportsystemrent.handlers.errors.service;

import org.ihaarr.accenture.transportsystemrent.dto.errors.BaseErrorMessageDto;
import org.ihaarr.accenture.transportsystemrent.exceptions.NotEnoughBalanceException;
import org.ihaarr.accenture.transportsystemrent.exceptions.TransportIsNotInRadiusException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TransportServiceErrorsHandler {

    @ExceptionHandler(TransportIsNotInRadiusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleNotEnoughBalanceException(TransportIsNotInRadiusException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }
}
