package org.ihaarr.accenture.transportsystemrent.handlers.errors;

import org.ihaarr.accenture.transportsystemrent.dto.errors.BaseErrorMessageDto;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.ParkingNotExistsException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.TransportNotExistsException;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.UserNotExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EntityNotExistsHandler {
    @ExceptionHandler(UserNotExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleUserNotExistsException(UserNotExistsException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(TransportNotExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleTransportNotExistsException(TransportNotExistsException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(ParkingNotExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleParkingNotExistsException(ParkingNotExistsException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }
}
