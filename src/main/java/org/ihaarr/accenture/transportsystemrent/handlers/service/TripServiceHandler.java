package org.ihaarr.accenture.transportsystemrent.handlers.service;

import org.ihaarr.accenture.transportsystemrent.dto.errors.BaseErrorMessageDto;
import org.ihaarr.accenture.transportsystemrent.exceptions.NotEnoughBalanceException;
import org.ihaarr.accenture.transportsystemrent.exceptions.TransportIsNotAvaiableException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TripServiceHandler {
    @ExceptionHandler(TransportIsNotAvaiableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleTransportIsNotAvaiableException(TransportIsNotAvaiableException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(NotEnoughBalanceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleNotEnoughBalanceException(NotEnoughBalanceException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }
}
