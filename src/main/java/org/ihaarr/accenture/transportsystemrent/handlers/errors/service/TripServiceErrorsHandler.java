package org.ihaarr.accenture.transportsystemrent.handlers.errors.service;

import org.ihaarr.accenture.transportsystemrent.dto.errors.BaseErrorMessageDto;
import org.ihaarr.accenture.transportsystemrent.exceptions.*;
import org.ihaarr.accenture.transportsystemrent.exceptions.entity.UserNotExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TripServiceErrorsHandler {
    @ExceptionHandler(AccessToClosedTripException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleAccessToClosedTripException(AccessToClosedTripException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(AccessToDifferentUserTripException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleAccessToDifferentUserTripException(AccessToDifferentUserTripException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(InvalidTransportTypeOnParkingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleInvalidTransportTypeOnParkingException(InvalidTransportTypeOnParkingException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(NotEnoughBalanceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleNotEnoughBalanceException(NotEnoughBalanceException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(TransportIsNotAvaiableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleTransportIsNotAvaiableException(TransportIsNotAvaiableException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }
}
