package org.ihaarr.accenture.transportsystemrent.handlers.errors;

import org.hibernate.PropertyValueException;
import org.ihaarr.accenture.transportsystemrent.dto.errors.BaseErrorMessageDto;
import org.ihaarr.accenture.transportsystemrent.exceptions.InvalidTransportTypeOnParkingException;
import org.ihaarr.accenture.transportsystemrent.exceptions.NoAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class RestControllerErrorHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, "Failed to deserialize json: " + e.getMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, String.format("Property %s: %s",e.getName(),
                e.getMessage()));
    }

    @ExceptionHandler(PropertyValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handlePropertyValueException(PropertyValueException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, String.format("%s: %s", e.getPropertyName(),
                e.getLocalizedMessage().split(":")[0]));
    }

    @ExceptionHandler(NoAccessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleNoAccessException(NoAccessException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(InvalidTransportTypeOnParkingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseErrorMessageDto handleInvalidTransportTypeOnParkingException(InvalidTransportTypeOnParkingException e) {
        return new BaseErrorMessageDto(HttpStatus.BAD_REQUEST, e.getMessage());
    }

}
