package org.ihaarr.accenture.transportsystemrent.repository;

import org.ihaarr.accenture.transportsystemrent.entity.transport.Transport;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportCondition;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportStatus;
import org.ihaarr.accenture.transportsystemrent.entity.transport.TransportType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransportRepository extends CrudRepository<Transport, Long> {
    @Query(value = "SELECT t FROM Transport t WHERE t.type = ?1")
    public Iterable<Transport> getTransportsByType(TransportType type);

    @Query(value = "SELECT t FROM Transport t WHERE t.condition = ?1")
    public List<Transport> getTransportsByCondition(TransportCondition condition);

    @Query(value = "SELECT t FROM Transport t WHERE t.status = ?1")
    public List<Transport> getTransportsByStatus(TransportStatus status);

    @Query(value = "SELECT t FROM Transport t WHERE t.status = 'FREE' AND t.type = ?1")
    public Iterable<Transport> getFreeTransportsByType(TransportType type);

    @Query(value = "SELECT t FROM Transport t WHERE t.status = 'FREE'")
    public Iterable<Transport> getFreeTransports();
}
