package org.ihaarr.accenture.transportsystemrent.repository;

import org.ihaarr.accenture.transportsystemrent.entity.parking.Parking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingRepository extends CrudRepository<Parking, Long> {
}
