package org.ihaarr.accenture.transportsystemrent.repository;

import org.ihaarr.accenture.transportsystemrent.entity.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    @Query("SELECT u FROM User u where u.login = ?1")
    public User findByLogin(String login);
}
