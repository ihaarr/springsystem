package org.ihaarr.accenture.transportsystemrent.repository;

import org.ihaarr.accenture.transportsystemrent.entity.trip.Trip;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {
    @Query(value = "SELECT t FROM Trip t WHERE t.user.id = ?1")
    public Iterable<Trip> findAllByUserId(Long id);

    @Query(value = "SELECT t FROM Trip t WHERE t.user.id = ?1 AND t.status = CREATED")
    public Iterable<Trip> findAllOpenByUserId(Long id);
}
